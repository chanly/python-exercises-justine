def factorial(n):
	result = 1
	for i in range(1, n + 1):
		result = result * i
		print result
	return result

print factorial(10)


#fibonacci
""" Returns a list of fibonacci sequence """
def fibonacci(n):
	if n < 1:
		raise ValueError('positive only')
	if n == 1:
		return[1]
	result = [1,1]

	for i in range(n - 2):
		result.append(result[-2] + result[-1])
	return result

print fibonacci(10)