
class Student(object):

	def __init__(self,name, subjects):
		self.name = name
		self.subjects = subjects

	def get_name(self):
		return self.name

	def get_subject(self):
		return self.subjects


class Teacher(object):

	def __init__(self,name, subjects):
		self.name = name
		self.subjects = subjects
		for subject in subjects:
			subject.add_teacher(self)

	def get_name(self):
		return self.name

	def get_subject(self):
		return self.subjects

	def add_subject(self, subject):
		self.subjects.append(subject)


class Subject(object):

	def __init__(self, name):
		self.name = name
		self.teacher = Teacher("None", [])

	def get_name(self):
		return self.name

	def add_student(self, student):

		self.students.append(student)
	def add_teacher(self, teacher):
		self.teacher = teacher

	def get_teacher(self):
		return self.teacher

	def __unicode__(self):
		return self.name

math = Subject("math")
language = Subject("language")
filipino = Subject("Filipino")
science = Subject("Science")
history = Subject("History")

jv = Student("jv", [math, science, language])
justine = Student("justine", [language, math])
chanly = Student("chanly",[language, math])


ogs = Teacher("ogs", [math, science])
dom = Teacher("dom", [language])
pat = Teacher("pat", [filipino])

print filipino.get_teacher().get_name()
print pat.get_subject()[0].get_name()

print
print jv.get_name(), ': '

subjects = jv.get_subject()
for subject in subjects:
	print ' ', subject.get_name()

