class Person(object):
	"""docstring for Person"""
	def __init__(self):
		pass
	def getGender(self):
		pass

class Male(Person):
	"""docstring for Male"""
	def __init__(self):
		pass
	def getGender(self):
		print "Male"

class Female(Person):
	"""docstring for Female"""
	def __init__(self):
		pass
	def getGender(self):
		print "Female"
		
f = Female()
f.getGender()

m = Male()
m.getGender()