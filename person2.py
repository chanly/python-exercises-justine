class Person(object):
	"""docstring for Person"""
	def __init__(self):
		pass
	def getGender(self):
		print self.gender		

class Male(Person):
	"""docstring for Male"""
	def __init__(self):
		self.gender = "Male"


class Female(Person):
	"""docstring for Female"""
	def __init__(self):
		self.gender = "Female"
		
f = Female()
f.getGender()

f = Male()
f.getGender()

